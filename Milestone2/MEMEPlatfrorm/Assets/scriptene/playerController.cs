﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerController : MonoBehaviour {

	
    public bool facingRight = true;

    public float walkspeed = 10.0f;

    // Use this for initialization

    // Update is called once per frame
    void Update()
    {
        float move = Input.GetAxis("Horizontal");

        if (move > 0) GetComponent<Rigidbody2D>().velocity = new Vector3(move * walkspeed, GetComponent<Rigidbody2D>().velocity.y);
        if (move < 0) GetComponent<Rigidbody2D>().velocity = new Vector3(move * walkspeed, GetComponent<Rigidbody2D>().velocity.y);

        if (move > 0 && facingRight) Flip();
        if (move < 0 && !facingRight) Flip();
    
    }
    void Flip()
    {
        facingRight = !facingRight;
        transform.Rotate(Vector3.up * 180);
    }
    GameObject player2D;


}