﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class stopwatchScript : MonoBehaviour {

    //variable for the number of laps
    public int numberoflaps;

    //array to store the laptimes
    float[] laps;

    //float to store the current time
    float currentTime;

    //references to the text inside Unity
    GameObject stopwatchtext, lapstext;

    //to stop / start the stopwatch
    bool stopwatchStarted;

    //index where I am going to save
    int lapindex;

    string formatTime(float theTime)
    {
        float minutes, seconds, milliseconds;
        string output = "";

        minutes = Mathf.Floor(theTime / 60f);
        seconds = theTime % 60f;
        milliseconds = seconds * 1000f;
        milliseconds = milliseconds % 1000f;

        output = minutes.ToString("00") + ":" + seconds.ToString("00") + ":"
            + milliseconds.ToString("000");

        return output;

    }


    // Use this for initialization
    void Start()
    {
        lapindex = 0;
        laps = new float[numberoflaps];
        currentTime = 0f;
        stopwatchtext = GameObject.Find("stopwatch");
        lapstext = GameObject.Find("laps");
        stopwatchStarted = false;
        lapstext.GetComponent<Text>().text = "";
        foreach (float eachlap in laps)
        {
            //each lap time is displayed under each other
            lapstext.GetComponent<Text>().text += formatTime(eachlap) + "\n";
        }

    }

    // Update is called once per frame
    void Update()
    {
        //start the stopwatch
        if (Input.GetKeyDown(KeyCode.T))
        {
            stopwatchStarted = !stopwatchStarted;
        }

        if (stopwatchStarted)
        {

            //implementing the lap button
            if (Input.GetKeyDown(KeyCode.B))
            {
                //if lapindex is 10, which means i've gone over the end of the array
                if (lapindex > laps.Length)
                {
                    lapindex = 0;
                }
                //save the current time
                laps[lapindex] = currentTime;
                lapstext.GetComponent<Text>().text = "";
                foreach (float eachlap in laps)
                {
                    //each lap time is displayed under each other
                    lapstext.GetComponent<Text>().text += formatTime(eachlap) + "\n";
                }
                lapindex++;
            }


            currentTime += Time.deltaTime;
            stopwatchtext.GetComponent<Text>().text = formatTime(currentTime);
        }


    }
}