﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreTextScript : MonoBehaviour {

    public Text scoreText;
    private int score;
    float timer = 1f;
    float Delay = 0;

    void Start()
    {
        score = 0;
        ScoreText();
    }

   

    void ScoreText()
    {
        scoreText.text = "Score: " + score.ToString();
    }
}